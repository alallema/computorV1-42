#!/usr/bin/env python

# import modules used here -- sys is a very standard one
from __future__ import print_function
import sys
import re
import collections
import numpy as np

#Error
ERR_NO_ARG = 'no argument'
ERR_ANY_EQ = 'any equality'
ERR_TOO_MANY_EQ = 'to many equality'
ERR_CAN = "I can't solve."

    ### +++++++++++ ###
    # +++ Parsing +++ #
    ### +++++++++++ ###

def check_number_of_argument(argument):
    if len(argument) == 0:
        print (ERR_NO_ARG)
    elif len(argument.split('=')) == 1:
        print (ERR_ANY_EQ)
    elif len(argument.split('=')) > 2:
        print (ERR_TOO_MANY_EQ)
    else:
        argument_to_equation(argument)

def sentence_to_tab(argument):
    if argument[0] != '+' and argument[0] != '-':
        argument = '+' + argument
    tabs = re.findall(r'[\+-][\d.]*\d+\*X\^[\+-]*[\d.]*\d+', argument)
    return (tabs)

#reg_polynom = compile('([-+=]?)\s*([0-9\.]+)?(\s*\*?\s*[xX](?:\s*\^\s*([0-9]+))?)?\s*')
#reg_space = compile('\s+')

def argument_to_sentence(argument):
    argument = argument.split(' ')
    argument = ''.join(argument)
    sentence = argument.split('=')
    return(sentence)

def extract_degree_coeff_from_tab(tabs):
    last = lambda a: re.findall(r'[\+-]*[\d.]*\d+', a)
    tabs = [last(tab) for tab in tabs]
    for value in tabs:
        value[0] = float(value[0])
    for tab in tabs:
        if sum(x.count(tab[1]) for x in tabs) > 1:
            values = np.array(tabs)
            searchval = tab[1]
            index = np.where(values == searchval)[0]
            tab[0] = tab[0] + tabs[index[1]][0]
            tabs.pop(index[1])
    tabs = {key: value for (value, key) in tabs}
    return (tabs)

    ### +++++++++++ ###
    # +++ Reduced +++ #
    ### +++++++++++ ###

def clean_dict(equation):
    int_dict = {}
    final_dict = {}
    for key, value in equation.items():
        if float(key).is_integer():
            int_dict[int(key)] = value
        else:
            return equation
    for key, value in int_dict.items():
        final_dict[str(key)] = value
    return final_dict

def reduced_equation(equation, reduced):
    equation = extract_degree_coeff_from_tab(equation)
    equation = clean_dict(equation)
    if reduced:
        reduced = extract_degree_coeff_from_tab(reduced)
        reduced = clean_dict(reduced)
        for degree in reduced:
            if degree in equation:
                equation[degree] = equation[degree] - reduced[degree]
            else:
                equation[degree] = -reduced[degree]
    equation_reduced = collections.OrderedDict(sorted(equation.items()))
    return equation_reduced

def print_reduced_form(equation):
    s = ''
    for degree in equation:
        buff = equation[degree]
        if buff >= 0:
            sign = ' + '
        else:
            sign = ' - '
            buff = -buff
        if buff.is_integer():
            buff = int(buff);
        s = s + sign + str(buff) + ' * X^' + str(degree)
    if s[1] == '+':
        s = s[2:]
    if s[1] == '-':
        s = ' ' + s[1] + s[3:]
    print ("Reduced form:" + s + ' = 0')

    ### ++++++++++++++++++++++ ###
    # +++ Polynomial_degree  +++ #
    ### ++++++++++++++++++++++ ###

def calcul_polynomial_degree(equation):
    polynomial_degree = equation.keys()[-1]
    for key in reversed(list(equation)):
        if polynomial_degree in equation and equation[polynomial_degree] == 0 and int(polynomial_degree) > 0:
            polynomial_degree = str(int(polynomial_degree) - 1)
        elif polynomial_degree not in equation:
            polynomial_degree = str(int(polynomial_degree) - 1)
    return polynomial_degree

def print_polynomial_degree(polynomial_degree):
    print ('Polynomial degree: ' + str(polynomial_degree))
    if int(polynomial_degree) > 2:
        print ('The polynomial degree is stricly greater than 2, ') + (ERR_CAN)
        return False
    else:
        return True

def check_degree_value(equation):
    for value in equation:
        if float(value).is_integer() == False :
            print ('The polynomial degree isn\'t integer, ' + ERR_CAN)
            return False
        elif int(value) < 0:
            print ('The polynomial degree is negative, ' + ERR_CAN)
            return False
        elif int(value) > 2:
            print ('The polynomial degree is stricly greater than 2, ' + ERR_CAN)
            return False
    return True

    ### ++++++++++++ ###
    # +++ Solution +++ #
    ### ++++++++++++ ###

def print_solution(solution):
    if solution.is_integer():
        print (int(solution), end='')
    else:
        print (float(solution), end='')


def calcul_solution(polynomial_degree, equation):
    if int(polynomial_degree) == 2:
        discriminant = equation['1'] * equation['1'] - 4 * equation['0'] * equation['2']
        if discriminant > 0:
            print ('Discriminant is strictly positive, the two solutions are:')
            print_solution(((-equation['1'])-(discriminant)**0.5)/(2*equation['2']))
            print ('')
            print_solution(((-equation['1'])+(discriminant)**0.5)/(2*equation['2']))
            print ('')
        elif discriminant < 0:
            print ('Discriminant is strictly negative, the two complex solutions are:')
            print_solution(((-equation['1'])-(abs(discriminant))**0.5)/(2*equation['2']))
            print ('i')
            print_solution(((-equation['1'])+(abs(discriminant))**0.5)/(2*equation['2']))
            print ('i')
        elif discriminant == 0:
            print ('Discriminant is nul, the two solutions are equal to: ')
            print_solution(-(equation['1']/2*equation['2']))
            print ('')
    if int(polynomial_degree) == 1:
        print ('The solution is: ')
        print_solution((-equation['0'])/equation['1'])
        print ('')
    if int(polynomial_degree) == 0 and equation['0']:
        print ("No solution, I can't solve.")
    elif int(polynomial_degree) == 0:
        print ("Every number is solution, I can't solve.")

def argument_to_equation(argument):
    sentence = argument_to_sentence(argument)
    equation = []
    reduced = []
    if '0' in sentence:
        sentence.remove('0')
    else:
        reduced = sentence_to_tab(sentence[1])
    equation = sentence_to_tab(sentence[0])
    equation = reduced_equation(equation, reduced)
    print_reduced_form(equation)
    if check_degree_value(equation):
        polynomial_degree = calcul_polynomial_degree(equation)
        if print_polynomial_degree(polynomial_degree):
            if not '0' in equation:
                equation['0'] = 0;
            if not '1' in equation:
                equation['1'] = 0;
            if not '2' in equation:
                equation['2'] = 0;
            calcul_solution(polynomial_degree, equation)

# Main() function
def main():
    argument = sys.argv[1].replace("\n", "")
    if len(sys.argv) == 2:
        check_number_of_argument(argument)
    elif len(sys.argv) == 1:
        print (ERR_NO_ARG)
    else:
        print ('to many arguments')

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
