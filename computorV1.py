#!/usr/bin/env python

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    computorV1.py                                      :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alallema <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/14 10:24:12 by alallema          #+#    #+#              #
#    Updated: 2018/11/14 10:54:40 by alallema         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# import modules used here -- sys is a very standard one
import sys
import re

    ### +++++++++++ ###
    # +++  Error  +++ #
    ### +++++++++++ ###

ERR_NO_ARG = 'No argument'
ERR_ANY_EQ = 'Any equality'
ERR_TOO_MANY_EQ = 'Too many equality'
ERR_TOO_MANY_ARG = 'Too many argument'
ERR_CAN = "I can't solve."

reg_number = lambda value: re.findall('([-+]?)\s*([0-9\.]+)?(\s*\*?\s*[xX](?:\s*\^\s*([\+-]*[0-9\.]+))?)?\s*', value)
regex = r"([^xX0-9-+=*^_.\s])"
sign_coeff = lambda value: -1 if value == '-' else 1 
def_sign = lambda value: '-' if value < 0 else '+'
int_num = lambda value: int(value) if value.is_integer() else value

class Number():
    sign = None
    power = 0
    coeff = 0.0
    X = False
    pos = 0

    def __init__(self, sign = None, coeff = 0.0, X = False, power = 0, pos = 0):
        self.sign = sign
        self.power = power
        self.coeff = coeff
        self.X = X
        self.pos = pos

    def __eq__(self, other):
        return self.Value == other.Value

    def __add__(self, other):
        self.coeff = self.coeff + other.coeff
        if other.X == True: 
            self.X = True

    def __str__(self):
        s = ''
        if self.coeff or self.X:
            if self.pos == 1 and self.coeff < 0:
                s = '-'
            if self.pos == 0:
                s = def_sign(self.coeff) + ' '
            s = s + str(abs(int_num(self.coeff)))
            if not self.X and self.power == 1:
                s = s + ' * X'
            if self.X:
                s = s + ' * X^'
            if self.X:
                s = s + str(int_num(self.power))
        return s

    def __repr__(self):
        return str(self)

    def definition(self, sign, coeff, X, power):
        self.sign = sign
    	self.coeff = float(coeff)
        self.X = True if X else False
        if self.X and not power:
            power = 1
            self.X = False
    	self.power = float(power) if power else 0.0

    def exist(self):
        exist = False
        if self.coeff:
            exist = True
        if self.power:
            exist = True
        return exist

class Polynome():
    equation = [] # for storing the Number instance 
    a = Number()
    b = Number()
    c = Number()
    discriminant = 0
    polynomial_degree = 0

    def __init__(self, equation = [], a = Number(), b = Number(), c = Number(), discriminant = 0, polynomial_degree = 0):
        self.equation = equation
        a.definition(None, 0.0, False, 2)
        b.definition(None, 0.0, False, 1)
        c.definition(None, 0.0, False, 0)
        self.discriminant = discriminant
        self.polynomial_degree = polynomial_degree

    def __str__(self):
        s = ''
        for number in self.equation:
            s = s + str(number) + ' '
        if s == ' ':
            return '' 
        s = s + '= 0'
        return s
    
    def __repr__(self):
        return str(self)
    
    def find_by_power(self, power):
        for number in self.equation:
           if number.power == power:
               return number
        return False

    def find_polynomial_degree(self):
        for number in reversed(self.equation):
            if number.coeff != 0:
                return number.power
        return 0.0

    def natural_form(self):
        for number in self.equation:
            if number.power == 0:
                self.c = number
            if number.power == 1:
                self.b = number
            if number.power == 2:
                self.a = number
        if self.c.X:
            self.b.X = True
        if self.b.X:
            self.c.X = True
        for number in self.equation:
            if number.power == 0:
                number.X = self.c.X
            if number.power == 1:
                number.X = self.b.X
    
    def reduced(self, argument, pos):
        for value in argument:
            number = Number()
            if value[1]:
                coeff = float(value[1]) * sign_coeff(value[0]) * pos
            else:
                coeff = 0
            number.definition(def_sign(coeff), coeff, value[2], value[3])
            if number.exist():
                buff = self.find_by_power(number.power)
                if buff:
                    buff = buff + number
                else:
                    self.equation.append(number)

    def check_reduce(self):
        self.polynomial_degree = self.find_polynomial_degree()
        print ('Polynomial degree: ' + str(int_num(self.polynomial_degree)))
        for number in self.equation:
            if not number.power.is_integer():
                print ('The polynome has float degree, ' + ERR_CAN)
                return False
            elif number.power < 0:
                print ('The polynomial has negative degree, ' + ERR_CAN)
                return False
            elif number.power > 2:
                print ('The polynomial degree is stricly greater than 2, ' + ERR_CAN)
                return False
        return True

    def resolve(self):
        if self.polynomial_degree == 2:
            self.discriminant = self.b.coeff ** 2 - 4 * self.a.coeff * self.c.coeff
            if self.discriminant > 0:
                print ('Discriminant is strictly positive, the two solutions are:')
                print (int_num((-self.b.coeff-(self.discriminant**0.5))/(2*self.a.coeff)))
                print (int_num((-self.b.coeff+(self.discriminant**0.5))/(2*self.a.coeff)))
            if self.discriminant < 0:
                print ('Discriminant is strictly negative, the two complex solutions are:')
                print (str(int_num((-self.b.coeff-(abs(self.discriminant)**0.5))/(2*self.a.coeff)))) + 'i'
                print (str(int_num((-self.b.coeff+(abs(self.discriminant)**0.5))/(2*self.a.coeff)))) + 'i'
            if self.discriminant == 0:
                print ('Discriminant is nul, the two solutions are equal to: ')
                print (int_num(-self.b.coeff/2*self.a.coeff))
        if self.polynomial_degree == 1:
            print ('The solution is: ')
            print (int_num(-self.c.coeff/self.b.coeff))
        if self.polynomial_degree == 0 and self.c.coeff:
            print ('No solution, I can\'t solve.')
        elif self.polynomial_degree == 0:
            print ("Every number is solution.")
        return True

class Computer():

    argument = []
    polynome = Polynome()

    def __init__(self):
        self.argument = []
    	self.equation = []
    	self.reduced = []

    def check_number_of_argument(self):
        if len(self.argument) == 0:
            print (ERR_NO_ARG)
        elif len(self.argument.split('=')) == 1:
            print (ERR_ANY_EQ)
        elif len(self.argument.split('=')) > 2:
            print (ERR_TOO_MANY_EQ)
        elif bool(re.search(regex, self.argument)):
            print 'Bad syntax'
        else:
            return True
        return False
  
    def parse(self, argument):
        self.argument = argument.replace('\n', '')
        if not self.check_number_of_argument():
            return False
        self.argument = re.sub('\s+', '', self.argument)
        self.argument = self.argument.split('=')
        if len(self.argument[0]) == 0 or len(self.argument[1]) == 0:
            print (ERR_ANY_EQ)
            return False
        self.argument = [reg_number(value) for value in self.argument]
        for tab in self.argument:
            for value in tab:
                if (len(set(value))==1):
                    tab.remove(value)
        return True

    def reduce(self):
        self.polynome.reduced(self.argument[0], 1)
        self.polynome.reduced(self.argument[1], -1)
        self.polynome.equation.sort(key=lambda x: x.power)
        self.polynome.equation[0].pos = 1
        self.polynome.natural_form()
        if str(self.polynome):
            print 'Reduced form: ' + str(self.polynome)
        return self.polynome.check_reduce()

    def resolve(self):
        if (self.polynome.resolve()):
            return True
        else:
            return False


# Main() function
def main():
    if len(sys.argv) == 2:
        argument = Computer()
        if not argument.parse(sys.argv[1]):
        	exit(1)
        if not argument.reduce():
        	exit(1)
        if not argument.resolve():
        	exit(1)

    elif len(sys.argv) == 1:
        print (ERR_NO_ARG)
    else:
        print (ERR_TOO_MANY_ARG)

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
